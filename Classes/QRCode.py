class QRCode:
    def __init__(self, raw_data, position=None, image=None):
        self.raw_data = raw_data
        self.position = position
        self.image = image
        data_list = raw_data.split(";")
        self.qr_code_version = data_list[0]
        self.kassen_seriennummer = data_list[1]
        self.process_type = data_list[2]
        self.process_data = data_list[3]
        self.transaktions_nummer = data_list[4]
        self.signatur_zaehler = data_list[5]
        self.start_zeit = data_list[6]
        self.log_time = data_list[7]
        self.sig_alg = data_list[8]
        self.log_time_format = data_list[9]
        self.signatur = data_list[10]
        self.public_key = data_list[11]
