from Classes.QRCode import QRCode


class TestImage:
    def __init__(self, image_path: str, qrcode: QRCode):
        data_list = image_path.removesuffix(".jpg").split("_")
        self.type = data_list[0]
        self.shop_name = data_list[1]
        self.date = data_list[2]
        self.color = data_list[3]
        self.qrcode = qrcode
