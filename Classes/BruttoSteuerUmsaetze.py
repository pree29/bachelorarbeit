class BruttoSteuerUmsaetze:
    def __init__(self, brutto_steuerumsaetze):
        bsu_list = brutto_steuerumsaetze.split("_")
        self.allgemeiner_steuersatz = float(bsu_list[0])
        self.ermaessigter_steuersatz = float(bsu_list[1])
        self.durchschnittssatz_paragraph24_nr3 = float(bsu_list[2])
        self.durchschnittssatz_paragraph24_nr1 = float(bsu_list[3])
        self.zero_percent = float(bsu_list[4])
