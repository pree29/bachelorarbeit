from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import hashes
from Classes.QRCode import QRCode
from datetime import datetime
import base64
import hashlib


def get_signature_algorithm(algorithm: str) -> ec.ECDSA:
    """
    This function matches the algorithm description of a qrcode with the algorithm function of cryptography.
    """
    match algorithm:
        case "ecdsa-plain-SHA256":
            return ec.ECDSA(hashes.SHA256())
        case "ecdsa-plain-SHA384":
            return ec.ECDSA(hashes.SHA384())
        case "ecdsa-plain-SHA512":
            return ec.ECDSA(hashes.SHA512())
        case "ecdsa-plain-SHA3-256":
            return ec.ECDSA(hashes.SHA3_256())
        case "ecdsa-plain-SHA3-384":
            return ec.ECDSA(hashes.SHA3_384())
        case "ecdsa-plain-SHA3-512":
            return ec.ECDSA(hashes.SHA3_512())


def get_signature_algorithm_oid(algorithm: str) -> str:
    """
    This function matches the algorithm description of a qrcode with the algorithm OID of the BSI.
    """
    match algorithm:
        case "ecdsa-plain-SHA256":
            return "0.4.0.127.0.7.1.1.4.1.3"
        case "ecdsa-plain-SHA384":
            return "0.4.0.127.0.7.1.1.4.1.4"
        case "ecdsa-plain-SHA512":
            return "0.4.0.127.0.7.1.1.4.1.5"
        case "ecdsa-plain-SHA3-256":
            return "0.4.0.127.0.7.1.1.4.1.9"
        case "ecdsa-plain-SHA3-384":
            return "0.4.0.127.0.7.1.1.4.1.10"
        case "ecdsa-plain-SHA3-512":
            return "0.4.0.127.0.7.1.1.4.1.11"


def get_public_key(public_key: str) -> ec.EllipticCurvePublicKey:
    """
    This function loads a public key of cryptography with the public key data of a qrcode.
    """
    possible_curves = [ec.SECP256R1(), ec.SECP384R1(), ec.SECP521R1(),
                       ec.BrainpoolP256R1(), ec.BrainpoolP384R1(), ec.BrainpoolP512R1()]
    loaded_public_key = None
    for curve in possible_curves:
        try:
            loaded_public_key = ec.EllipticCurvePublicKey.from_encoded_point(curve, base64.b64decode(public_key))
        except ValueError:
            continue
    return loaded_public_key


def get_time_formatted(date: str, date_format: str):
    """
    This function formats the date of a qrcode to the specified date format.
    """
    match date_format:
        case "unixTime":
            try:
                return int(datetime.fromisoformat(date).timestamp())
            except ValueError:
                return int(datetime.strptime(date, "%Y-%m-%dT%H:%M:%S.%f%z").timestamp())


def get_tse_serial_number(qrcode: QRCode):
    """
    This function calculated the TSE serial number from a qrcode.
    """
    return hashlib.sha256(base64.b64decode(qrcode.public_key))
