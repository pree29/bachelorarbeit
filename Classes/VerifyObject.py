from Classes.QRCode import QRCode
from Classes import VerificationParameters
import base64


class VerifyObject:
    def __init__(self, qrcode: QRCode = None):
        if qrcode is None:
            self.public_key = None
            self.algorithm = None
            self.signature = None
            self.message = None
        elif type(qrcode) is QRCode:
            self.public_key = VerificationParameters.get_public_key(qrcode.public_key)
            self.algorithm = VerificationParameters.get_signature_algorithm(qrcode.sig_alg)
            self.signature = base64.b64decode(qrcode.signatur)
            self.message = f"version||certifiedDataType||certifiedData||serialNumber||" \
                           f"signatureAlgorithm||seAuditData||signatureCounter||logTime".encode('utf-8')
