from Classes.BruttoSteuerUmsaetze import BruttoSteuerUmsaetze


class ProcessData:
    def __init__(self, process_type, process_data):
        match process_type:
            case "Kassenbeleg-V1":
                pd_list = process_data.split("^")
                self.vorgangstyp = pd_list[0]
                self.brutto_steuerumsaetze = BruttoSteuerUmsaetze(pd_list[1])
                self.zahlungen = pd_list[2]
