import logging
import asn1
from Classes.VerifyObject import VerifyObject
from Classes.QRCode import QRCode
from Services.VerifySignatureService import VerifySignatureService
from Classes import VerificationParameters


class TestASN1DER:
    def __init__(self):
        pass

    @staticmethod
    def load_qrcodestring_from_file(path: str) -> str:
        """
        This function loads a string in the format as contained in a qrcode from a text file.
        """
        with open(path, 'rb') as file:
            qrcodestring = file.read()
        return qrcodestring.decode("utf-8")

    @staticmethod
    def load_log_message(path: str) -> bytes:
        """
        This function loads a log message of a TSE.
        """
        with open(path, 'rb') as file:
            logbytes = file.read()
        return logbytes

    @staticmethod
    def print_asn_message(bytestream: bytes) -> None:
        """
        This function prints a message, encoded in ASN1 DER.
        """
        print("\n---Message in ASN1---")
        decoder = asn1.Decoder()
        decoder.start(bytestream)
        for i in range(11):
            t, v = decoder.read()
            print(t)
            print(v)

    @staticmethod
    def print_asn_log_message(bytestream: bytes) -> None:
        """
        This function prints a TSE log message, loaded by the load_log_message() function.
        """
        print("\n---Logmessage in ASN1---")
        decoder = asn1.Decoder()
        decoder.start(bytestream)
        tag, value = decoder.read()
        print(tag)
        print(value)
        decoder.start(value)
        for i in range(12):
            t, v = decoder.read()
            print(t)
            print(v)

    @staticmethod
    def make_message_asn1_der_encoded(bytestream: bytes) -> (bytes, bytes):
        """
        This function makes a ASN1 DER encoded signature message of a TSE log message,
        loaded by the load_log_message() function.
        """
        encoder = asn1.Encoder()
        decoder = asn1.Decoder()
        encoder.start()
        decoder.start(bytestream)
        tag, value = decoder.read()
        decoder.start(value)
        for i in range(11):
            tag, value = decoder.read()
            encoder.write(value, tag.nr, tag.typ, tag.cls)
        tag, signature = decoder.read()
        message = encoder.output()

        return message, signature

    def test_asn1_message(self, log_bytestream: bytes, qrcode: QRCode) -> None:
        """
        This function makes and tries to verify a ASN1 DER encoded signature message,
        loaded by the load_log_message() function.
        The associated qrcode is required because the log message does not contain a public key.
        """
        message, signature = self.make_message_asn1_der_encoded(log_bytestream)
        service = VerifySignatureService()
        verify_object = VerifyObject()
        verify_object.message = message
        verify_object.signature = signature
        verify_object.algorithm = VerificationParameters.get_signature_algorithm(qrcode.sig_alg)
        verify_object.public_key = VerificationParameters.get_public_key(qrcode.public_key)
        logging.info(f"asn1 message test | {service.verify_signature(verify_object)} | {message}")

    def test_asn1_message_with_sequence(self, log_bytestream: bytes, qrcode: QRCode) -> None:
        """
        This function makes and tries to verify a ASN1 DER encoded signature message in a SEQUENCE,
        loaded by the load_log_message() function.
        The associated qrcode is required because the log message does not contain a public key.
        """
        message, signature = self.make_message_asn1_der_encoded(log_bytestream)
        encoder = asn1.Encoder()
        encoder.start()
        encoder.write(message, 16, 32, 0)
        service = VerifySignatureService()
        message = encoder.output()
        verify_object = VerifyObject()
        verify_object.message = message
        verify_object.signature = signature
        verify_object.algorithm = VerificationParameters.get_signature_algorithm(qrcode.sig_alg)
        verify_object.public_key = VerificationParameters.get_public_key(qrcode.public_key)
        logging.info(f"asn1 message test with SEQUENCE| {service.verify_signature(verify_object)} | {message}")
