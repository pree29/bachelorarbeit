from Classes.QRCode import QRCode
from Services.VerifySignatureService import VerifySignatureService
from Classes import VerificationParameters
import logging


class PossibleMessageFields:
    def __init__(self, qrcode: QRCode):
        self.version = [b'2']
        self.certified_data_type = [b'bsi-de (0.4.0.127.0.7) applications (3) sE-API (7) sE-API-dataformats(1) 1',
                                    b'id-SE-API-transaction-log',
                                    b'0.4.0.127.0.7.3.7.1.1']
        self.operation_type = [b'FinishTransaction']
        self.client_id = [b'%b' % qrcode.kassen_seriennummer.encode('utf-8')]
        self.process_data = [b'%b' % qrcode.process_data.encode('utf-8')]
        self.process_type = [b'%b' % qrcode.process_type.encode('utf-8')]
        self.transaction_number = [b'%b' % qrcode.transaktions_nummer.encode('utf-8')]
        self.serial_number = [b'%b' % VerificationParameters.get_tse_serial_number(qrcode).hexdigest().encode('utf-8'),
                              b'%b' % VerificationParameters.get_tse_serial_number(qrcode).hexdigest().upper()
                              .encode('utf-8'),
                              VerificationParameters.get_tse_serial_number(qrcode).digest()]
        self.signature_algorithm = [b'%b' % qrcode.sig_alg.encode('utf-8'),
                                    b'%b' % VerificationParameters.get_signature_algorithm_oid(qrcode.sig_alg)
                                    .encode('utf-8')]
        self.signature_counter = [b'%b' % qrcode.signatur_zaehler.encode('utf-8')]
        self.log_time = [b'%b' % qrcode.log_time.encode('utf-8'),
                         b'%i' % VerificationParameters.get_time_formatted(qrcode.log_time, qrcode.log_time_format),
                         b'%i' % (VerificationParameters.get_time_formatted(qrcode.log_time, qrcode.log_time_format)
                                  - 3600),
                         b'%i' % (VerificationParameters.get_time_formatted(qrcode.log_time, qrcode.log_time_format)
                                  - 7200),
                         b'%i' % (VerificationParameters.get_time_formatted(qrcode.log_time, qrcode.log_time_format)
                                  + 3600),
                         b'%i' % (VerificationParameters.get_time_formatted(qrcode.log_time, qrcode.log_time_format)
                                  + 7200)]

    def try_every_message(self, qrcode: QRCode, concatenation: str, verify_signature_service: VerifySignatureService)\
            -> None:
        """
        This function forms every message that can be formed from this class and tries to verify them.
        The results are in the test_log.txt.
        """
        verify_object = verify_signature_service.convert_qrcode_to_verify_object(qrcode)
        parameter_list = [b''] * 11
        for v in self.version:
            parameter_list[0] = v
            for cdt in self.certified_data_type:
                parameter_list[1] = cdt
                for ot in self.operation_type:
                    parameter_list[2] = ot
                    for ci in self.client_id:
                        parameter_list[3] = ci
                        for pd in self.process_data:
                            parameter_list[4] = pd
                            for pt in self.process_type:
                                parameter_list[5] = pt
                                for tn in self.transaction_number:
                                    parameter_list[6] = tn
                                    for sn in self.serial_number:
                                        parameter_list[7] = sn
                                        for sa in self.signature_algorithm:
                                            parameter_list[8] = sa
                                            for sc in self.signature_counter:
                                                parameter_list[9] = sc
                                                for lt in self.log_time:
                                                    parameter_list[10] = lt
                                                    message = b''
                                                    for p in parameter_list:
                                                        if p == lt:
                                                            message += p
                                                        else:
                                                            message += p + concatenation.encode('utf-8')
                                                    verify_object.message = message
                                                    verified = verify_signature_service.verify_signature(verify_object)
                                                    logging.info(f"message test | {verified} | {message}")
