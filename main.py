import cv2
from flask import Flask, request
import logging
from Services.MainService import MainService
import numpy

app = Flask(__name__)


@app.route("/check_qrcode_image", methods=["POST"])
def check_qrcode_image() -> str:
    file = request.files['image'].read()
    numpy_image = numpy.frombuffer(file, numpy.uint8)
    image = cv2.imdecode(numpy_image, cv2.IMREAD_COLOR)
    return str(service.check_qrcode_image(image))


@app.route("/check_qrcode_string", methods=["POST"])
def check_qrcode_string() -> str:
    qrcodestring = request.args.get("qrcodestring")
    return str(service.check_qrcode_string(qrcodestring))


if __name__ == "__main__":
    logging.basicConfig(filename="log.txt", level=logging.INFO,
                        format="%(asctime)s | %(levelname)s | %(message)s")
    logging.info('Started')
    service = MainService()
    app.run(debug=True)
    logging.info('Finished')
