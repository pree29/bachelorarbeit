import numpy

from Services.QRCodeService import QRCodeService
from Services.VerifySignatureService import VerifySignatureService
from Services.ReceiptService import ReceiptService
from Classes.TestASN1DER import TestASN1DER
from Classes.PossibleMessageFields import PossibleMessageFields
from Classes.QRCode import QRCode
import logging
import cv2
import os
from Classes.TestImage import TestImage


class MainService:
    def __init__(self):
        self.qrcode_service = QRCodeService()
        self.verify_signature_service = VerifySignatureService()
        self.receipt_service = ReceiptService()

    # test functions

    def test_load_all_test_images_qrcode(self, folder_path: str, size: int = 500) -> [TestImage]:
        """
        This test function loads all images from a folder and load the qrcodes.
        The results are in the text file test_log.txt.
        """
        images = []
        image_pathes = os.listdir(folder_path)
        for image_path in image_pathes:
            logging.info(f"try loading image | {image_path}")
            image = cv2.imread(os.path.join(folder_path, image_path))
            if image is None:
                logging.error(f"failed loading image | {image_path}")
                continue
            qrcode = self.qrcode_service.get_qrcode(image, size)
            test_image = TestImage(image_path, qrcode)
            images.append(test_image)

        blue = 0
        white = 0
        loaded_qrcodes_blue = 0
        loaded_qrcodes_white = 0

        for image in images:
            if image.color == "Weiss":
                white += 1
                if image.qrcode is not None:
                    loaded_qrcodes_white += 1
            if image.color == "Blau":
                blue += 1
                if image.qrcode is not None:
                    loaded_qrcodes_blue += 1

        logging.info(f"loaded images {loaded_qrcodes_blue + loaded_qrcodes_white} / {len(images)}")
        logging.info(f"loaded blue receipts {loaded_qrcodes_blue} / {blue}")
        logging.info(f"loaded white receipts {loaded_qrcodes_white} / {white}")
        return images

    def test_run_image(self, image: numpy.ndarray):
        """
        This test function tries to verify a signature message of an image.
        The result is in the text file test_log.txt.
        """
        qrcode = self.qrcode_service.get_qrcode(image)
        verify_object = self.verify_signature_service.convert_qrcode_to_verify_object(qrcode)
        verified = self.verify_signature_service.verify_signature(verify_object)
        logging.info(f"test run image | {verified} | {verify_object.message}")

    def try_every_message(self, image: numpy.ndarray, concatenation: str):
        """
        This test function tries every possible signature message defined in PossibleMessageFields of an image.
        The results are in the text file test_log.txt.
        """
        qrcode = self.qrcode_service.get_qrcode(image)
        possible_message_fields = PossibleMessageFields(qrcode)
        possible_message_fields.try_every_message(qrcode, concatenation, self.verify_signature_service)

    @staticmethod
    def try_verify_asn1_logmessage(qrcode_path, log_path):
        """
        This test function tries to verify the signature message made by a TSE log message.
        The result is in the text file test_log.txt.
        """
        asn1_tester = TestASN1DER()
        log_message = asn1_tester.load_log_message(log_path)
        qrcode_string = asn1_tester.load_qrcodestring_from_file(qrcode_path)
        qrcode = QRCode(qrcode_string)
        asn1_tester.test_asn1_message(log_message, qrcode)
        asn1_tester.test_asn1_message_with_sequence(log_message, qrcode)

    # main functions

    def check_qrcode_image(self, image: numpy.ndarray) -> bool:
        """
        This function check the digital signature of an image, send through the API.
        """
        qrcode = self.qrcode_service.get_qrcode(image)
        verify_object = self.verify_signature_service.convert_qrcode_to_verify_object(qrcode)
        verified = self.verify_signature_service.verify_signature(verify_object)
        logging.info(f"verified image with qrcode | {verified}")
        return verified

    def check_qrcode_string(self, qrcodestring: str) -> bool:
        """
        This function check the digital signature of a qrcode string, send through the API.
        """
        qrcode = QRCode(qrcodestring)
        verify_object = self.verify_signature_service.convert_qrcode_to_verify_object(qrcode)
        verified = self.verify_signature_service.verify_signature(verify_object)
        logging.info(f"verified qrcodestring | {verified}")
        return verified
