from Classes.VerifyObject import VerifyObject
from Classes.QRCode import QRCode
from pprint import pprint
from cryptography.exceptions import InvalidSignature


class VerifySignatureService:
    def __int__(self):
        pass

    @staticmethod
    def print_verify_object(verify_object: VerifyObject) -> None:
        """
        This function prints the data of a verify object.
        """
        if verify_object is None:
            return None
        print("\n---Verify Object---")
        pprint(vars(verify_object))

    @staticmethod
    def convert_qrcode_to_verify_object(qrcode: QRCode) -> VerifyObject:
        """
        This function convert a qrcode to a verify object.
        """
        return VerifyObject(qrcode)

    @staticmethod
    def verify_signature(verify_object: VerifyObject) -> bool:
        """
        This function verify the signature of a verify object.
        """
        if verify_object.signature is None or verify_object.message is None \
                or verify_object.algorithm is None or verify_object.public_key is None:
            return "No QR-Code detected"
        is_signature_correct = False
        try:
            verify_object.public_key.verify(
                signature=verify_object.signature,
                data=verify_object.message,
                signature_algorithm=verify_object.algorithm
            )
            is_signature_correct = True
        except InvalidSignature:
            pass
        return is_signature_correct
