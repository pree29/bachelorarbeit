import cv2
import numpy
import pyzbar.pyzbar as pyzbar
from pyzbar.pyzbar import ZBarSymbol
import logging
from Classes.QRCode import QRCode
from pprint import pprint
from copy import deepcopy


class QRCodeService:
    def __int__(self):
        pass

    def get_pyzbar_qrcodes(self, image, size: int = 500) -> list:
        """
        This function loads the qrcodes from an image.
        """
        image = self.resize_image(image, size)
        # image = self.optimize_image(image)
        return pyzbar.decode(image, symbols=[ZBarSymbol.QRCODE])

    @staticmethod
    def convert_pyzbar_qrcode_list_to_qrcode(qrcodes, image) -> QRCode:
        """
        This functions tries to find the right qrcode and convert it to the own qrcode data type.
        """
        for qrcode in qrcodes:
            try:
                return QRCode(qrcode.data.decode("utf-8"), qrcode.polygon, image)
            except IndexError:
                continue
        logging.error("can not find a qrcode in image")
        return None

    def get_qrcode(self, image: numpy.ndarray, size: int = 500) -> QRCode:
        """
        This function tries to find a qrcode in an image.
        """
        if image is None:
            logging.error("image is empty")
            return None
        try:
            qrcodes = self.get_pyzbar_qrcodes(image, size)
            qrcode = self.convert_pyzbar_qrcode_list_to_qrcode(qrcodes, image)
            return qrcode
        except IndexError:
            return None

    def show_qrcode(self, qrcode: QRCode):
        """
        This function shows a picture with qrcode and a bounding box around the found qrcode.
        """
        if qrcode is None:
            return
        image = self.resize_image(qrcode.image)
        if qrcode.position is not None:
            for i in range(len(qrcode.position)):
                pt1 = [int(val) for val in qrcode.position[i]]
                pt2 = [int(val) for val in qrcode.position[(i + 1) % 4]]
                cv2.line(image, pt1, pt2, color=(255, 0, 0), thickness=10)

            image = self.resize_image(image)
            # image = self.optimize_image(image)
            cv2.imshow('Detected QR code', image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

    @staticmethod
    def resize_image(image: numpy.ndarray, size: int = 500) -> numpy.ndarray:
        """
        This function resizes an image for a better chance to find the qrcode or showing the picture.
        """
        if image is None:
            return None
        (h, w) = image.shape[:2]
        if h > w:
            width = size
            height = int(width / w * h)
            return cv2.resize(image, (width, height))
        else:
            height = size
            width = int(height / h * w)
            return cv2.resize(image, (width, height))

    @staticmethod
    def print_qrcode(qrcode: QRCode):
        """
        This function prints the data of a qrcode.
        """
        if qrcode is None:
            return
        print("\n---QRCode---")
        temp = deepcopy(qrcode)
        try:
            temp.process_data.brutto_steuerumsaetze = vars(temp.process_data.brutto_steuerumsaetze)
            temp.process_data = vars(temp.process_data)
        except TypeError:
            pass
        temp.image = None
        pprint(vars(temp))

    @staticmethod
    def optimize_image(image: numpy.ndarray) -> numpy.ndarray:
        """
        This function optimizes an image for a better chance to find the qrcode.
        """
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        (thresh, black_and_white_image) = cv2.threshold(gray_image, 127, 255, cv2.THRESH_BINARY)
        return black_and_white_image
