from Services.MainService import MainService
import logging
import cv2
import os


def main():
    try:
        os.remove("test_log.txt")
    except FileNotFoundError:
        pass
    logging.basicConfig(filename="test_log.txt", level=logging.INFO,
                        format="%(asctime)s | %(levelname)s | %(message)s")
    logging.info('Started')
    service = MainService()

    # Load all test images from folder and read qrcodes
    folder_path_qrcodes = r'TestImages\QRCodes'
    folder_path_receipts = r'TestImages\ReceiptWithQRCode'
    service.test_load_all_test_images_qrcode(folder_path_qrcodes, 1000)
    service.test_load_all_test_images_qrcode(folder_path_receipts, 2000)

    # Try every message with concatenation symbol
    test_path = r'TestImages\QRCodes\QRCode_Buchlounge_202207041201_Weiss.jpg'
    image = cv2.imread(test_path)
    service.try_every_message(image, ";")
    service.try_every_message(image, "||")

    # Try ASN1 message from TSE log message

    '''
    Not functional because of missing files due to missing permission to publish them
    
    qrcode_path = "QRCode.txt"
    log_path = "Unixt_0000000000_Sig-520848_Log-Tra_No-144997_Finish_Client-00000000-0000-0000-0000-000000.log"
    service.try_verify_asn1_logmessage(qrcode_path, log_path)
    '''

    logging.info('Finished')


if __name__ == '__main__':
    main()
