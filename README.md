# Verifikation von digitalen Signaturen auf Kassenzetteln in Deutschland
Dieses Projekt enthält den Code zu der Bachelorarbeit mit dem oben stehenden Titel von Erik Prendke.

## Technologie
Es wurden folgende Elemente bei der Entwicklung genutzt: 
* Pycharm Community Edition 2022.1.3
* Python 3.10
* Postman 10.6.0

## Benötigte Frameworks
Das Programm besitzt Abhängigkeiten zu folgenden Frameworks:
* Flask
* Cryptography
* Opencv
* Pyzbar
* Future
* Asn1

## Framework Installation
Die Frameworks können wie folgt mit z. B. pip installiert werden:
```
$pip install cryptography Flask opencv-python pyzbar future asn1
```

## Ausführung 
### Test
Die enthaltene test.py führt verschiedene Testfunktionen aus.
Die Ergebnisse des Tests werden in der test_log.txt Datei ausgegeben.
```
$ python test.py
```

### Main
Die enthaltene main.py started eine Flask API auf einem Flask Entwicklungsserver
```
$ python main.py
```
#### check_qrcode_image
Erwartet eine Bilddatei eines QRCodes im Body eines Http POST Request. <br>
Endpoint: <br>
http://127.0.0.1:5000/check_qrcode_image

#### check_qrcode_string
Erwartet einen String im Format des Strings, der in einem QRCode enthalten ist. Wird in einem Http POST Request als Parameter verschickt. <br>
Endpoint: <br>
http://127.0.0.1:5000/check_qrcode_string
